const express = require('express');
const mongoose = require('mongoose');
const app = express()

const cors = require('cors')

app.use(cors())

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas!"))
mongoose.connect(`mongodb+srv://admin:bZYlUhQPHYj7oGLp@j3s-0.0vwt5.mongodb.net/booking_system_db?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//we need to define the routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)

app.listen(4000, () => {
	console.log("API is Now ONLINE"); 
})